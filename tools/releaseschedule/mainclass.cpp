#include "mainclass.h"
#include "ui_mainclass.h"

#include <QApplication>
#include <QCryptographicHash>
#include <QDebug>

MainClass::MainClass(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainClass)
{
    ui->setupUi(this);
    connect( ui->generateButton, SIGNAL(clicked()),
             SLOT( slotGenerateTechbase() ) );
    connect( ui->icalButton, SIGNAL(clicked()),
             SLOT( slotGenerateICal() ) );
}

MainClass::~MainClass()
{
    delete ui;
}

QString MainClass::explanation( Events event )
{
    QString desc;
    switch ( event ) {
        case depfreeze :
                desc = "From this moment on it is not allowed to add new dependencies or bump dependencies versions. It is possible to get an exception for this. Post the patch to reviewboard and add the release-team as reviewer. We will check if the dependency is needed and is available on all platforms. In other words, if you have a feature that requires a new dependency or a version of a dependency that is higher than currently checked for in the build system, you need to have committed this change before this date.";
                break;
        case freezebetatagrelease :
                desc = "Only bugfixes from this point on. Bugfixes needing user visible text changes need approval from kde-i18n-doc@kde.org. Beta is tagged (tag name is vXX.YYPREV.80). As soon as the tarballs have been confirmed to build and the Release Team thinks they meet enough quality it will be released.";
                break;
        case rctagrelease :
                desc = "Release Candidate is tagged (tag name is vXX.YYPREV.90). As soon as the tarballs have been confirmed to build and the Release Team thinks they meet enough quality it will be released.";
                break;
        case finaltag :
                desc = "The branch is frozen for final release tagging (tag name is vXX.YY.0). Only urgent fixes, such as those fixing compilation errors, should be committed.";
                break;
        case finalrelease :
                desc = "Final release is released for general consumption.";
                break;
        case minortag :
                desc = "A KDE minor release is tagged and made available to the packagers.";
                break;
        case minorrelease :
                desc = "A KDE minor release is released to the public.";
                break;
        default :
                desc = "Unknown freeze, don't honor it :)";
            }
    return desc;
}

QString MainClass::title( Events event, const QString& version )
{
    QString mainVersion(ui->versionEdit->text());
    QString desc;
    switch ( event ) {
        case depfreeze :
                desc = "Dependency Freeze";
                break;
        case freezebetatagrelease :
                desc = "Freeze and Beta Tag and Release";
                break;
        case rctagrelease :
                desc = "RC Tagging and Release";
                break;
        case finaltag :
                desc = "Tagging";
                break;
        case finalrelease :
                desc = "Release";
                break;
        case minortag :
                mainVersion += '.'+version;
                desc = QString("tagging");
                break;
        case minorrelease :
                mainVersion += '.'+version;
                desc = QString("release");
                break;
        default :
                desc = "Unknown freeze, don't honor it :)";
            }

    desc = QString( "KDE Applications %1 %2" ).arg( mainVersion ).arg( desc );
    return desc;
}

QPair<QString, QString> MainClass::makePair( Events event, const QString& version )
{
    return qMakePair( title( event, version ), explanation( event ) );
}

QMultiMap<QDate, QPair<QString, QString> > MainClass::generateTimeline()
{
    QMultiMap<QDate, QPair<QString, QString> > timeline;
    QDate release = ui->releaseDate->date();
    timeline.insert( release, makePair( finalrelease ) );

    QDate timelinePoint( release );
    timelinePoint = timelinePoint.addDays( ui->tagBeforeRelease->value() * -1 );
    timeline.insert(  timelinePoint, makePair( finaltag ) );

    timeline.insert( timelinePoint.addDays( ui->dependencyFreeze->value() * -7 ),
                     makePair( depfreeze ) );

    timeline.insert( timelinePoint.addDays( ui->freezeBeta->value() * -7 ),
                     makePair( freezebetatagrelease ) );

    timeline.insert( timelinePoint.addDays( ui->rcTaggingRelease->value() * -7 ),
                     makePair( rctagrelease ) );

    // Minor releases - currently only 3
    QDate minorDate = ui->releaseDate->date();
    minorDate = minorDate.addDays(ui->point1Release->value());
    for (int i = 1 ; i <= 3; i++) {
        timeline.insert( minorDate.addDays(5), makePair(minorrelease, QString::number(i) ));
        timeline.insert( minorDate, makePair(minortag, QString::number(i) ));
        minorDate = minorDate.addDays( 35 );
    }

    return timeline;

}

void MainClass::slotGenerateTechbase()
{
    // Needed for tag names vXX.(YY-1)
    QString mainVersion(ui->versionEdit->text());
    QString versionX = mainVersion.section('.', 0, 0);
    QString versionY = mainVersion.section('.', 1, 1);
    QString versionprevY = versionY;
    bool ok;
    int versionYminus1 = versionY.toInt(&ok, 10);
    if (ok) {
        versionprevY = QString::number(versionYminus1 - 1);
    }

    QMultiMap<QDate, QPair<QString, QString> > timeline = generateTimeline();

    QLocale english( QLocale::English );     // Dates in english
    QString text;
    QMap<QDate, QPair<QString, QString> >::const_iterator i;
    for (i = timeline.constBegin(); i != timeline.constEnd(); ++i) {
        text.append( "=== " + english.toString( i.key() ) + ": " + i.value().first + " ===\n");
        QString desc(i.value().second);
        desc.replace("XX", versionX);
        desc.replace("YYPREV", versionprevY);
        desc.replace("YY", versionY);
        desc.replace('\n',' ');
        text.append(desc + "\n\n" );
    }

    ui->schedule->setText( text );
}

void MainClass::slotGenerateICal()
{
    QDate dnow = QDate::currentDate();

    // Needed for tag names vXX.(YY-1)
    QString mainVersion(ui->versionEdit->text());
    QString versionX = mainVersion.section('.', 0, 0);
    QString versionY = mainVersion.section('.', 1, 1);
    QString versionprevY = versionY;
    bool ok;
    int versionYminus1 = versionY.toInt(&ok, 10);
    if (ok) {
        versionprevY = QString::number(versionYminus1 - 1);
    }

    QMultiMap<QDate, QPair<QString, QString> > timeline = generateTimeline();
    QLocale english( QLocale::English );     // Dates in english
    QString text;
    text.append( "BEGIN:VCALENDAR\r\n" );
    text.append( "VERSION:2.0\r\n");
    text.append( "PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n\r\n");

    QMap<QDate, QPair<QString, QString> >::const_iterator i;
    for (i = timeline.constBegin(); i != timeline.constEnd(); ++i) {
        text.append( "BEGIN:VEVENT\r\n");
        QDateTime dt( i.key() );
        text.append( "DTSTAMP:" + dnow.toString( "yyyyMMdd" ) + "T000000Z\r\n" );
        text.append( "DTSTART;VALUE=DATE:" + dt.toString( "yyyyMMdd" ) + "\r\n" );
        text.append( "DTEND;VALUE=DATE:" + dt.addDays(1).toString( "yyyyMMdd" ) + "\r\n" );
        text.append( "X-MICROSOFT-CDO-ALLDAYEVENT:TRUE\r\n" );
        text.append( "X-MICROSOFT-CDO-BUSYSTATUS:FREE\r\n" );
        text.append( "X-MICROSOFT-CDO-INTENDEDSTATUS:FREE\r\n" );
        text.append( "SUMMARY:" + i.value().first + "\r\n" );
        QString desc(i.value().second);
        desc.replace("XX", versionX);
        desc.replace("YYPREV", versionprevY);
        desc.replace("YY", versionY);
        desc.replace('\n',' ');
        text.append( "DESCRIPTION:" + desc + "\r\n" );
        QCryptographicHash md5( QCryptographicHash::Md5 );
        md5.addData( i.value().first.toLatin1() );
        text.append( "UID:" + md5.result().toHex() + "\r\n" );
        text.append( "END:VEVENT\r\n\r\n");

    }

    text.append( "END:VCALENDAR\r\n");
    ui->schedule->setText( text );
}
